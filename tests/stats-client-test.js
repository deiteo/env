"use strict";

var assert = require('assert');
var mockConfig = require('./mock-config');
var statsClient = require('../lib/stats-client');

describe('gitter-stats-client', function() {

  it('should fire off all the events correctly', function() {
    process.env.JOB = 'test-1';

    var config = mockConfig({
      stats: {
        statsd: {
          enabled: true
        }
      }
    });

    var client = statsClient.create({ config: config, tags: ['test:1'] });
    assert(client);
    assert(client.global_tags.indexOf('test:1') >= 0);
  });

});
