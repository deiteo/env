'use strict';

var handlebars = require('handlebars').create();
var fs = require('fs');
var Promise = require('bluebird');

function compiler(sourceFileName) {
  return Promise.fromCallback(function(callback) {
    fs.readFile(sourceFileName, 'utf-8', callback);
  })
  .then(function(source) {
    return handlebars.compile(source);
  })
}

module.exports = compiler;
