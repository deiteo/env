"use strict";

function configureNodeEnv() {
  var nodeEnv = process.env.NODE_ENV;
  if (nodeEnv) return nodeEnv;

  /* Default to NODE_ENV=dev */
  process.env.NODE_ENV = 'dev';
  return 'dev';
}

/* Factory */
exports.create = function(configDirectory) {
  var nconf = require('nconf');
  var path = require('path');
  var EventEmitter = require('events').EventEmitter;
  var Promise = require('bluebird');

  var SYSTEM_CONFIG_DIR = '/etc/gitter/';

  /* Load configuration parameters */
  var nodeEnv = configureNodeEnv();

  var staging = process.env.STAGING;
  staging = staging === '1' || staging === 'true';

  var configDir = configDirectory;
  if(process.env.GITTER_CONFIG_DIR_OVERRIDE) {
    configDir = process.env.GITTER_CONFIG_DIR_OVERRIDE;
    console.log("Overridden config directory: " + configDir);
  }

  // Monkey-patch an event-emitter onto nconf (for now)
  nconf.events = new EventEmitter();

  nconf.argv()
       .env('__');

  nconf.add('envUser', { type: 'file', file: path.join(configDir, 'config.' + nodeEnv + '.user-overrides.json') });

  nconf.add('user', { type: 'file', file: path.join(configDir, 'config.user-overrides.json') });

  if(staging) {
    nconf.add('systemStagingOverrides', { type: 'file', file: path.join(SYSTEM_CONFIG_DIR, 'config.' + nodeEnv + '-staging.overrides.json') });
  }

  nconf.add('systemOverrides', { type: 'file', file: path.join(SYSTEM_CONFIG_DIR, 'config.' + nodeEnv + '.overrides.json') });

  if(staging) {
    nconf.add('nodeEnvStaging', { type: 'file', file: path.join(configDir, 'config.' + nodeEnv + '-staging.json') });
  }

  nconf.add('nodeEnv', { type: 'file', file: path.join(configDir, 'config.' + nodeEnv + '.json') });
  nconf.add('systemDefaults', { type: 'file', file: path.join(SYSTEM_CONFIG_DIR, 'config.' + nodeEnv + '.json') });

  nconf.add('defaults', { type: 'file', file: path.join(configDir, 'config.default.json') });

  process.on('SIGHUP', function() {
    return Promise.map(
      [
        nconf.stores.user,
        nconf.stores.nodeEnv,
        nconf.stores.defaults
      ], function(store) {
        return Promise.fromCallback(function(callback) {
          return store.load(callback);
        });
      })
      .then(function() {
        nconf.events.emit('reload');
        return null;
      })
      .catch(function(err) {
        console.log('gitter-config: Reload failed: ' + err, err);
      });
  });

  nconf.runtimeEnvironment = nodeEnv;

  function parseBool(value) {
    switch(typeof value) {
      case 'boolean':
        return value;
      case 'number':
        return !!value;
      case 'string':
        return !!JSON.parse(value);
      default:
        return undefined;
    }
  }

  /**
   * Monkey-patch `getBool`
   */
  nconf.getBool = function(key) {
    var value = this.get(key);
    return parseBool(value);
  }


  return nconf;
};
