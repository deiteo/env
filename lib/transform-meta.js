/* jshint node:true, unused:true */
'use strict';

function transformMeta(meta, depth) {
  if (typeof meta !== 'object') {
    return meta;
  }

  if(!meta) return;

  if(!depth) depth = 0;

  // eslint-disable-next-line complexity
  function transformValue(key, value) {
    if(value === undefined) return;

    if(value instanceof Error) {
      return { message: value.message, stack: value.stack, name: value.name };
    }
    // Handle the case where it is supposed to be in the error shape but isn't an error object
    else if(key === 'error') {
      return { message: value, stack: null, name: null };
    }

    var valType = typeof value;

    if(valType === 'string' || valType === 'number' || valType === 'boolean') {
      return value;
    }

    /* Deal with null object values */
    if(!value) return null;

    /* Deal with mongo objectIds so we dont get binary logged */
    if(valType === 'object' && value._bsontype === 'ObjectID') {
      return "" + value;
    }

    if(depth < 1) {
      return transformMeta(value, depth + 1);
    }

    if(value.toString && typeof value.toString === 'function' && value.toString !== Object.prototype.toString) {
      return value.toString();
    }
  }

  var result = Object.keys(meta).reduce(function(result, key) {
    var value = transformValue(key, meta[key]);
    if(value !== undefined) {
      result[key] = value;
    }

    return result;
  }, {});

  return result;
}

module.exports = transformMeta;
